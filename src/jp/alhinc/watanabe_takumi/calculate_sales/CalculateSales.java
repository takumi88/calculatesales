package jp.alhinc.watanabe_takumi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		HashMap<String,String> branchNameMap = new HashMap<String,String>(); //支店定義ファイルのデータの保管（支店コード、支店名）
		HashMap<String,Long> branchSalesMap = new HashMap<String,Long>(); ///valueの値をlong型の０にした、支店定義ファイルのデータの保管
		BufferedReader br = null;

		if(args.length !=1 ) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		//①支店定義ファイル読み込み（メソッド分け済み）
		if(!inputBranchFile(args[0],"branch.lst","支店定義","[0-9]{3}",branchNameMap,branchSalesMap)){
			return;
		}

		//②集計
		File file1 = new File(args[0]);
		File[] list  = file1.listFiles();  //この2行でファイルの一覧を取得
		ArrayList<String> uriageFile = new ArrayList<String>();

		for(int i = 0; i<list.length; i++) {
			if(list[i].isFile() && list[i].getName().matches("[0-9]{8}.rcd")){ //&&で条件を追加（ファイルの一覧をStringに変更、正規表現を使って条件を指定）
				uriageFile.add(list[i].getName());
			}
		}

		for(int i = 0; i<uriageFile.size()-1; i++) {
			String message = uriageFile.get(i);
			String result = message.substring(0,8); //①ファイル名から.rcd以外を抜き出す
			Long uriageFileLong = Long.parseLong(result); //②抜き出した文字列を数値に変換
		//	Long UriagefileLong = Long.parseLong(uriageFile.get(i).substring(0,8)); //上３文が1行にまとまる


			String message2 = uriageFile.get(i+1);
			String result2 = message2.substring(0,8); //①.rcd以外の数字を計算するために2個目を抜き出す
			Long uriageFileLong2 = Long.parseLong(result2); //②数値に変換

			if((uriageFileLong+1) - uriageFileLong2 != 0) { //③数値同士（2つ）を比較する
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		for(int i = 0; i<uriageFile.size(); i++) {
			try {
				//売上ファイルのデータの読み込み
				File file2 = new File(args[0],uriageFile.get(i));
				FileReader gr = new FileReader(file2);
				br = new BufferedReader(gr);
				ArrayList<String> uriageData = new ArrayList<String>();

				String salesData;
				while((salesData = br.readLine()) != null){
					uriageData.add(salesData); //SalesDataは売上ファイルの支店コードと売上額（ArrayListに格納）
				}

				if(!branchNameMap.containsKey(uriageData.get(0))) {
					System.out.println(uriageFile.get(i) +"の支店コードが不正です");
					return;
				}

				if(uriageData.size() != 2) { //売上ファイルの支店コードと売上額
					System.out.println(uriageFile.get(i) +"のフォーマットが不正です");
					return;
				}

				if(!uriageData.get(1).matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				Long uriageValueLong = Long.parseLong(uriageData.get(1)); //売上ファイルの売上額をlong型に変換
				long totalSales =  branchSalesMap.get(uriageData.get(0)) + uriageValueLong;
				//long型の変数を作る。1つ目のmapの売上額０ と 2つ目のマップの売上額をlong型に変換したものの合計

				if(String.valueOf(totalSales).length() > 10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				branchSalesMap.put(uriageData.get(0) , totalSales);

			}catch(IOException g) {
				System.out.println("予期せぬエラーが発生しました");
			}finally {
				if(br != null) {
					try {
						br.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
					}
				}
			}
		}
		//③集計結果出力(メソッド分け済み)
		if(!outputBranchFile(args[0], "branch.out",branchNameMap, branchSalesMap)) {
			return;
		}
	}

	//メソッド分け（集計結果の出力）
	public static boolean outputBranchFile(String path, String filename, Map<String,String>branchName, Map<String,Long>branchSales) {
		BufferedWriter bw = null;
		try {
			File file3 = new File(path,filename);
			FileWriter fw = new FileWriter(file3);
			bw = new BufferedWriter(fw);

			for(String nKey : branchName.keySet()) {
				bw.write("" +nKey + ","+ branchName.get(nKey) +","+ branchSales.get(nKey));
				bw.newLine();
			}
		}catch(IOException g) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	//メソッド分け（支店定義ファイルの読み込み）
	public static boolean inputBranchFile(String path, String fileName,String shitenteigi, String branchFileCode, Map<String,String>branchName, Map<String,Long>branchSales) {
		BufferedReader br = null;
		try {
			File file = new File(path,fileName);

			if(!file.exists()) {
				System.out.println(shitenteigi + "ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
				while((line = br.readLine()) != null) {
					String[] Filedata = line.split(",");

					if(Filedata.length != 2) { //支店定義ファイルの配列の要素数が2つじゃないときにエラー文を出す
						System.out.println(shitenteigi + "ファイルのフォーマットが不正です");
						return false;
					}
					branchName.put(Filedata[0],Filedata[1]);
					branchSales.put(Filedata[0],0L); //0の後にLを入れることで、0をString型からlong型にしている
				}

				for(Map.Entry<String,String>hoge : branchName.entrySet()) {
					if(!hoge.getKey().matches(branchFileCode)){ //支店コードが3桁じゃない時にエラー文を出す
						System.out.println(shitenteigi + "ファイルのフォーマットが不正です");
						return false;
					}
				}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}

